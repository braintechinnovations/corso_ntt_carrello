	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <style>
        .active{
            font-weight: bolder;
        }

        .active a{
            color: blue !important;
        }
    </style>
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="admin.jsp">ADMIN</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="admin.jsp">Elenco Prodotti</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="inserisciprodotto.jsp">Aggiungi prodotto</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout">LogOut</a>
            </li>
            </ul>
        </div>
    </nav>
    
	
    
    