<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    	HttpSession sessione = request.getSession();
    	//String ruolo_utente = (String)sessione.getAttribute("role") != null ? (String)sessione.getAttribute("role") : "";
    	
    	//Metodo equivalente all'operatore ternario
    	String ruolo_utente = "";
    	if((String)sessione.getAttribute("role") != null){
    		ruolo_utente = (String)sessione.getAttribute("role");
    	}
    	
    	if(!ruolo_utente.equals("ADMIN"))
    		response.sendRedirect("errore.jsp?tipo_errore=NOTALLOWED");
    %>

    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>    <title>Elenco Prodotti</title>
</head>
<body>
    <%@ include file="adminmenu.jsp" %>

    <div class="container">

        <div class="row mt-5">
            <div class="col">
                <h1>Inserisci Prodotto</h1>
                <p>Compila il seguente form e clicca sul tasto <strong>SALVA</strong></p>
            </div>
        </div>
        
        
        <div class="row mt-5">
            <div class="col">

				<form name="modifica_prodotto" action="gestioneprodotto" method="POST">
					<div class="form-group">
			            <label for="input_codice">Codice</label>
			            <input type="text" class="form-control" name="input_codice"/>
			        </div>
			       
					<div class="form-group">
			            <label for="input_nome">Nome</label>
			            <input type="text" class="form-control" name="input_nome"/>
			        </div>
			        
					<div class="form-group">
			            <label for="input_prezzo">Prezzo</label>
			            <input type="number" class="form-control" name="input_prezzo"/>
			        </div>
			        
					<div class="form-group">
			            <label for="input_quantita">Quantita</label>
			            <input type="number" class="form-control" name="input_quantita" />
			        </div>
			        
			        <input type="text" name="type" value="insert"/>
			        <button type="submit" class="btn btn-success" name="btn_inserimento">SALVA</button>
		        </form>

            </div>
        </div>
        
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>