	<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <style>
        .active{
            font-weight: bolder;
        }

        .active a{
            color: blue !important;
        }
    </style>
	
	<%
		String uri = request.getRequestURI();
		String ctx = request.getContextPath();
		
		//out.println("Questo � URI: " + uri);
		//out.println("Questo � CONTEXT: " + ctx);
	%>
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.jsp">Ecommerceino</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
	            <li class="nav-item <% if(uri.contains("index.jsp")) { out.print("active"); } %>">
	                <a class="nav-link" href="index.jsp">Lista Prodotti</a>
	            </li>
	            <li class="nav-item <% if(uri.contains("login.jsp")) { out.print("active"); } %>">
	                <a class="nav-link" href="login.jsp">Autenticati</a>
	            </li>
	            <li class="nav-item <% if(uri.contains("carrello.jsp")) { out.print("active"); } %>">
	                <a class="nav-link" href="carrello.jsp">Carrello</a>
	            </li>
	            <li class="nav-item">
	                <a class="nav-link" href="logout">LogOut</a>
	            </li>
            </ul>
        </div>
    </nav>
    
	
    
    