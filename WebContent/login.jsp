<%@page import="com.lezione27.hw.dao.ProdottoDAO"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lezione27.hw.classi.Prodotto"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="com.lezione27.hw.connessione.ConnettoreDB"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    	/* Redirect se sono gi� loggato */
    
    	HttpSession sessione = request.getSession();
    	String ruolo_utente = (String)sessione.getAttribute("role") != null ? (String)sessione.getAttribute("role") : "";
    	
		if(ruolo_utente.equals("ADMIN"))
    		response.sendRedirect("admin.jsp");
		
		if(ruolo_utente.equals("SIMPLE"))
    		response.sendRedirect("simple.jsp");
    	
    %>
    
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Login</title>
</head>
<body>
    
    <%@ include file="menu.jsp" %>

    <div class="container">

        <div class="row mt-5">
       	 	<div class="col-md-3"></div>
            <div class="col-md-6 text-center">
                <h1>Login</h1>
                <p>Effettua l'accesso inserendo nome utente e password</p>
            </div>
       	 	<div class="col-md-3"></div>
        </div>
        
        <div class="row mt-5">
            <div class="col-3"></div>
            <div class="col-6">

				<form name="form_login" action="verificalogin" method="POST">
					<div class="form-group">
			            <label for="input_username">Username</label>
			            <input type="text" class="form-control" name="input_username"/>
			        </div>
			        <div class="form-group">
			            <label for="input_password">Password</label>
			            <input type="password" class="form-control" name="input_password"/>
			        </div>
			        
			        <button type="button" class="btn btn-success" name="btn_inserimento">LogIn</button>
				</form>
    
            </div>
            <div class="col-3"></div>
        </div>
        
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="js/script.js"></script>
</body>
</html>