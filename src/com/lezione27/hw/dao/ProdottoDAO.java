package com.lezione27.hw.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione27.hw.classi.Prodotto;
import com.lezione27.hw.connessione.ConnettoreDB;

public class ProdottoDAO {

	public ArrayList<Prodotto> findAll() throws SQLException{
       	ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT prodottoid, nome, codi, prez, quan FROM prodotto";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Prodotto temp = new Prodotto();
       		temp.setProdottoId(risultato.getInt(1));
       		temp.setProdottoNome(risultato.getString(2));
       		temp.setProdottoCodice(risultato.getString(3));
       		temp.setProdottoPrezzo(risultato.getFloat(4));
       		temp.setProdottoQuantita(risultato.getInt(5));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}
	
	public Prodotto findByCodice(String varCodice) throws SQLException{
       	ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT prodottoid, nome, codi, prez, quan FROM prodotto WHERE codi = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, varCodice);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();
   		Prodotto temp = new Prodotto();
   		temp.setProdottoId(risultato.getInt(1));
   		temp.setProdottoNome(risultato.getString(2));
   		temp.setProdottoCodice(risultato.getString(3));
   		temp.setProdottoPrezzo(risultato.getFloat(4));
   		temp.setProdottoQuantita(risultato.getInt(5));
       	
       	return temp;
	}
	
	public boolean delete(String varCodice) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "DELETE FROM prodotto WHERE codi = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, varCodice);
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
       	return false;
	}
	
	public boolean update(Prodotto objProdotto) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "UPDATE Prodotto SET nome = ?, prez = ?, quan = ? WHERE codi = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, objProdotto.getProdottoNome());
       	ps.setFloat(2, objProdotto.getProdottoPrezzo());
       	ps.setInt(3, objProdotto.getProdottoQuantita());
       	ps.setString(4, objProdotto.getProdottoCodice());

       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
       	return false;
	}
	
	public void insert(Prodotto objProdotto) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "INSERT INTO Prodotto (nome, codi, prez, quan) VALUE (?,?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, objProdotto.getProdottoNome());
       	ps.setString(2, objProdotto.getProdottoCodice());
       	ps.setFloat(3, objProdotto.getProdottoPrezzo());
       	ps.setInt(4, objProdotto.getProdottoQuantita());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	
       	objProdotto.setProdottoId(risultato.getInt(1));
	}
	
}
