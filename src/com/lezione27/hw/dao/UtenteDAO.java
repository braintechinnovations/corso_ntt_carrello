package com.lezione27.hw.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.lezione27.hw.classi.Utente;
import com.lezione27.hw.connessione.ConnettoreDB;

public class UtenteDAO {

	public void checkCredentials(Utente objUtente) throws SQLException {
		
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT utenteid, nome, pass, tipo FROM utente WHERE nome = ? AND pass = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, objUtente.getUserNome());
       	ps.setString(2, objUtente.getUserPass());
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	objUtente.setUserId(risultato.getInt(1));
       	//Ignoro i dati che sono gi� nell'oggetto utente passatomi!
       	objUtente.setUserTipo(risultato.getString(4));
		
	}
	
}
