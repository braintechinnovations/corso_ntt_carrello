package com.lezione27.hw.classi;

public class Prodotto {

	private Integer 	prodottoId;
	private String 		prodottoNome;
	private String 		prodottoCodice;
	private Float 		prodottoPrezzo;
	private Integer 	prodottoQuantita;
	
	public Prodotto(){		//public necessario perch� all'interno di un package!
		
	}
	
	public Integer getProdottoId() {
		return prodottoId;
	}
	public void setProdottoId(Integer prodottoId) {
		this.prodottoId = prodottoId;
	}
	public String getProdottoNome() {
		return prodottoNome;
	}
	public void setProdottoNome(String prodottoNome) {
		this.prodottoNome = prodottoNome;
	}
	public String getProdottoCodice() {
		return prodottoCodice;
	}
	public void varQuantitaProdotto(String prodottoCodice) {
		this.prodottoCodice = prodottoCodice;
	}
	public Float getProdottoPrezzo() {
		return prodottoPrezzo;
	}
	public void setProdottoPrezzo(Float prodottoPrezzo) {
		this.prodottoPrezzo = prodottoPrezzo;
	}
	public Integer getProdottoQuantita() {
		return prodottoQuantita;
	}
	public void setProdottoQuantita(Integer prodottoQuantita) {
		this.prodottoQuantita = prodottoQuantita;
	}
	public void setProdottoCodice(String prodottoCodice) {
		this.prodottoCodice = prodottoCodice;
	}

	public String toStringHtmlTable() {
		String  risultato = "<tr>";
					risultato += "<td>" + this.prodottoCodice + "</td>";
					risultato += "<td>" + this.prodottoNome + "</td>";
					risultato += "<td>" + this.prodottoPrezzo + "</td>";
					risultato += "<td>" + this.prodottoQuantita + "</td>";
				risultato += "</tr>";
		
		return risultato;
	}
	

	public String toStringHtmlTableEdit() {
		String  risultato = "<tr>";
					risultato += "<td>" + this.prodottoCodice + "</td>";
					risultato += "<td>" + this.prodottoNome + "</td>";
					risultato += "<td>" + this.prodottoPrezzo + "</td>";
					risultato += "<td>" + this.prodottoQuantita + "</td>";
					risultato += "<td><a href='gestioneprodotto?type=delete&codice=" + this.prodottoCodice + "'><i class=\"fas fa-trash\"></i></a></td>";	//PER LA GET!
					risultato += "<td><a href='modificaprodotto.jsp?codice=" + this.prodottoCodice + "'><i class=\"fas fa-pencil\"></i></a></td>";	//PER LA GET!

//					risultato += "<td>"
//							+ "<form action='gestioneprodotto' method='POST'>"
//							+ "<input type='text' name='codice' value='" + this.prodottoCodice + "' disabled hidden/>"
//							+ "<input type='text' name='type' value='delete' disabled hidden/>"
//							+ "<button type='submit' class='btn btn-primary'><i class=\"fas fa-trash\"></i></button>"
//							+ "</form>"
//							+ "</td>";
				risultato += "</tr>";
		
		return risultato;
	}
	
	public String toStringHtmlTableSimple() {
		String  risultato = "<tr data-codice='" + this.prodottoCodice + "'>";
					risultato += "<td><button class='btn btn-primary btn-sm inserimento'><i class='fas fa-shopping-cart'></i></button> " + this.prodottoCodice + "</td>";
					risultato += "<td>" + this.prodottoNome + "</td>";
					risultato += "<td>" + this.prodottoPrezzo + "</td>";
					risultato += "<td>" + this.prodottoQuantita + "</td>";
				risultato += "</tr>";
		
		return risultato;
	}
	
}
