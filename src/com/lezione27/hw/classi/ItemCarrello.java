package com.lezione27.hw.classi;

public class ItemCarrello {

	private String codiceProdotto;
	private int quantitaProdotto;
	
	public ItemCarrello() {
		
	}
	
	public ItemCarrello(String codiceProdotto, int quantitaProdotto) {
		this.codiceProdotto = codiceProdotto;
		this.quantitaProdotto = quantitaProdotto;
	}

	public String getCodiceProdotto() {
		return codiceProdotto;
	}
	public void setCodiceProdotto(String codiceProdotto) {
		this.codiceProdotto = codiceProdotto;
	}
	public int getQuantitaProdotto() {
		return quantitaProdotto;
	}
	public void setQuantitaProdotto(int quantitaProdotto) {
		this.quantitaProdotto = quantitaProdotto;
	}
	
	public String toStringHtmlTable() {
		String  risultato = "<tr>";
					risultato += "<td>" + this.codiceProdotto + "</td>";
					risultato += "<td>" + this.quantitaProdotto + "</td>";
				risultato += "</tr>";
		
		return risultato;
	}
	
	
}
