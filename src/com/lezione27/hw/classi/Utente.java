package com.lezione27.hw.classi;

public class Utente {

	private Integer userId;
	private String userNome;
	private String userPass;
	private String userTipo;
	
	public Utente() {
		
	}
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserNome() {
		return userNome;
	}
	public void setUserNome(String userNome) {
		this.userNome = userNome;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public String getUserTipo() {
		return userTipo;
	}
	public void setUserTipo(String userTipo) {
		this.userTipo = userTipo;
	}
	
	
	
	
	
}
