package com.lezione27.hw.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lezione27.hw.classi.Prodotto;
import com.lezione27.hw.dao.ProdottoDAO;

/**
 * Servlet implementation class GestioneProdotto
 */
@WebServlet("/gestioneprodotto")
public class GestioneProdotto extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sessione = request.getSession();
		String role = (String)sessione.getAttribute("role") != null ? (String)sessione.getAttribute("role") : "";
		
		if(!role.equals("ADMIN")) {
			response.sendRedirect("errore.jsp?tipo_errore=NOTALLOWED");
			return;
		}
			
		String tipoOperazione = request.getParameter("type") != null ? request.getParameter("type") : "";
		
		ProdottoDAO gestore = new ProdottoDAO();
		
		String varCodiceProdotto = null;
		String varNomeProdotto = null;
		Float varPrezzoProdotto = null;
		Integer varQuantitaProdotto = null;
		
		switch(tipoOperazione) {
		case "delete":
			String codiceProdotto = request.getParameter("codice") != null ? request.getParameter("codice") : "";
			try {
				if(gestore.delete(codiceProdotto))
					response.sendRedirect("admin.jsp");
				else
					response.sendRedirect("errore.jsp?tipo_errore=NOTEXECUTED");
			} catch (SQLException e) {
				response.sendRedirect("errore.jsp?tipo_errore=NOTEXECUTED");
				System.out.println(e.getMessage());
			}
			break;
		case "update":
			varCodiceProdotto = request.getParameter("input_codice") != null ? request.getParameter("input_codice") : "";
			varNomeProdotto = request.getParameter("input_nome") != null ? request.getParameter("input_nome") : "";
			varPrezzoProdotto = request.getParameter("input_prezzo") != null ? new Float(request.getParameter("input_prezzo")) : null;
			varQuantitaProdotto = request.getParameter("input_quantita") != null ? new Integer(request.getParameter("input_quantita")) : null;

			Prodotto temp = new Prodotto();
	   		temp.setProdottoNome(varNomeProdotto);
	   		temp.setProdottoCodice(varCodiceProdotto);
	   		temp.setProdottoPrezzo(varPrezzoProdotto);
	   		temp.setProdottoQuantita(varQuantitaProdotto);
	   		
	   		try {
				gestore.update(temp);
				response.sendRedirect("admin.jsp");
			} catch (SQLException e) {
				response.sendRedirect("errore.jsp?tipo_errore=NOTEXECUTED");
				System.out.println(e.getMessage());
			}
			
			break;
		case "insert":
			varCodiceProdotto = request.getParameter("input_codice") != null ? request.getParameter("input_codice") : "";
			varNomeProdotto = request.getParameter("input_nome") != null ? request.getParameter("input_nome") : "";
			varPrezzoProdotto = request.getParameter("input_prezzo") != null ? new Float(request.getParameter("input_prezzo")) : null;
			varQuantitaProdotto = request.getParameter("input_quantita") != null ? new Integer(request.getParameter("input_quantita")) : null;
			
			Prodotto temp_ins = new Prodotto();
			temp_ins.setProdottoNome(varNomeProdotto);
			temp_ins.setProdottoCodice(varCodiceProdotto);
			temp_ins.setProdottoPrezzo(varPrezzoProdotto);
			temp_ins.setProdottoQuantita(varQuantitaProdotto);
	   		
	   		try {
				gestore.insert(temp_ins);
			} catch (SQLException e) {
				response.sendRedirect("errore.jsp?tipo_errore=NOTEXECUTED");
				System.out.println(e.getMessage());
				return;
			}
	   		
	   		if(temp_ins.getProdottoId() != null) {
				response.sendRedirect("admin.jsp");
	   		}
	   		else {
				response.sendRedirect("errore.jsp?tipo_errore=NOTINSERTED");
	   		}
			break;
		default: 			
			response.sendRedirect("admin.jsp");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
