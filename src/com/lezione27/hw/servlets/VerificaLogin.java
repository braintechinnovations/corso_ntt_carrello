package com.lezione27.hw.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lezione27.hw.classi.Utente;
import com.lezione27.hw.dao.UtenteDAO;

/**
 * Servlet implementation class VerificaLogin
 */
@WebServlet("/verificalogin")
public class VerificaLogin extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.jsp?tipo_errore=NOGET");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String user = request.getParameter("input_username") != null ? request.getParameter("input_username") : "";
		String pass = request.getParameter("input_password") != null ? request.getParameter("input_password") : "";
		
		Utente utente = new Utente();
		utente.setUserNome(user);
		utente.setUserPass(pass);
		
		UtenteDAO utente_dao = new UtenteDAO();
		try {
			utente_dao.checkCredentials(utente);
		} catch (SQLException e) {
			
		} finally {
			if(utente.getUserId() != null) {

				HttpSession sessione = request.getSession();
				
				if(utente.getUserTipo().equals("ADMIN")) {
					sessione.setAttribute("role", "ADMIN");
					response.sendRedirect("admin.jsp");
					return;
				}
				
				if(utente.getUserTipo().equals("SIMPLE")) {
					sessione.setAttribute("role", "SIMPLE");
					response.sendRedirect("index.jsp");
					return;
				}
				
				 response.sendRedirect("errore.jsp?tipo_errore=NOROLE");
			}
			else {
				response.sendRedirect("errore.jsp?tipo_errore=NOUSER");
			}
		}
		
		
	}

}
