package com.lezione27.hw.servlets;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lezione27.hw.classi.ItemCarrello;

@WebServlet("/gestionecarrello")
public class GestioneCarrello extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("errore.jsp?tipo_errore=NOTALLOWED");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sessione = request.getSession();
		String tipologia = request.getParameter("tipo_operazione") != null ? request.getParameter("tipo_operazione") : "";
 		
		switch(tipologia) {
		case "inserimento":
			
			String codice = request.getParameter("input_codice") != null ? request.getParameter("input_codice") : "";
			Integer quantita = request.getParameter("input_quantita") != null ? new Integer(request.getParameter("input_quantita")) : null;
			

			Gson jsonizzatore = new Gson();
			ArrayList<ItemCarrello> elenco;

			String carrello_salvato = (String) sessione.getAttribute("carrello");

			if(sessione.getAttribute("carrello") == null)
				elenco = new ArrayList<ItemCarrello>();
			else {
				Type listType = new TypeToken<ArrayList<ItemCarrello>>(){}.getType();
				elenco = jsonizzatore.fromJson(carrello_salvato, listType);
			}
			
			if(quantita > 0) {
				ItemCarrello item = new ItemCarrello(codice, quantita);
				elenco.add(item);

				//String oggetto = new Gson().toJson(item);		//Modalit� compatta
				
				String itemJson = jsonizzatore.toJson(elenco);
				
				if(sessione.getAttribute("carrello") == null) {			//Nel caso in cui l'attributo carrello non esista nella sessione
					System.out.println("Ho creato l'attributo carrello nella sessione");
				}
				else {
					System.out.println("L'attributo carrello esiste gi�");
				}
				
				sessione.setAttribute("carrello", itemJson);		//Creo o sovrascrivo la sessione

				response.sendRedirect("carrello.jsp");
			}
			else {
				response.sendRedirect("errore.jsp?tipo_errore=QTYERROR");
			}

			
			break;
		case "eliminazione":
			break;
		}
		
	}

}
