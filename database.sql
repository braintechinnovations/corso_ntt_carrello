DROP DATABASE IF EXISTS lez_27_carrello;
CREATE DATABASE lez_27_carrello;
USE lez_27_carrello;


CREATE TABLE Prodotto(
	prodottoId INTEGER NOT NULL AUTO_INCREMENT, 
	nome VARCHAR(150) NOT NULL,
	codi VARCHAR(150) UNIQUE,
	prez FLOAT NOT NULL,
	quan INTEGER DEFAULT(0),
	PRIMARY KEY (prodottoId)
);

CREATE TABLE Utente (
	utenteId INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome VARCHAR(50) NOT NULL UNIQUE,
    pass VARCHAR(50) NOT NULL,
    tipo VARCHAR(6) CONSTRAINT CHECK (tipo IN("ADMIN", "SIMPLE"))
);

INSERT INTO Prodotto(nome, codi, prez, quan) VALUES 
("LATTE","LAT123456",1.20,120),
("BISCOTTI","BIS123457",1.80,80),
("GNUTELLA","GNUT123457",4.20,25);

INSERT INTO Utente (nome, pass, tipo) VALUES
("Giovanni", "1234", "ADMIN"),
("Mario", "1234", "ADMIN"),
("Vittorio", "1234", "SIMPLE"),
("Valeria", "1234", "SIMPLE");

SELECT * FROM Prodotto;
SELECT * FROM Utente;

